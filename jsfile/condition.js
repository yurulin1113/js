var a = 10,
  b = 20,
  c = 30;

a > b ? console.log("a大") : console.log("b大");

var max = a > b ? a : b;
console.log(max);

var max = a > b ? (a > c ? a : c) : b > c ? b : c; //不推薦
console.log(max);

var result = 1 || (2 && 3);
console.log(result);
