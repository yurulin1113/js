var obj = new Object();

obj.name = "孫悟空";
obj.age = "24";

obj.sayname = function () {
  document.write(obj.name);
};

function fun() {
  document.write(obj.name);
}

obj.sayname(); //調用方法

document.write("<br>");

fun();

document.write("<br>");
//------------------------------
var obj2 = {
  name: "悟飯",
  age: 24,
  gender: "男",
  address: "龜仙島",
};

//取屬性和值
for (var n in obj2) {
  document.write(n + ":" + obj2[n] + "<br>");
}
