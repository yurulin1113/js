var obj = new Object();

obj.name = "孫悟空";
obj.gender = "男";
obj.age = 18;
//delete obj.name;
console.log(obj);

document.writeln(obj.name + "<br>");
document.writeln("-------------------------<br> ");

obj["hello"] = "你好";

document.writeln(obj["hello"]);

document.writeln("<br>-------------------------<br> ");

var n = "hello";
document.writeln(obj[n]);

document.writeln("<br>-------------------------<br> ");

obj.test = true;
obj.test = null;
obj.test = undefined;

var obj2 = new Object();
obj2.name = "豬八戒";
obj.test = obj2;
document.writeln(obj.test.name);

document.writeln("<br>-------------------------<br> ");

document.writeln("test" in obj);
document.writeln("test2" in obj);
