function sum(a, b) {
  var c = a + b;
  return c;
}
var result = sum(9, 8);
document.write(result);

document.write("<br>");
//----------------------
function isadd(num) {
  return num % 2 == 0;
}
var num = isadd(200);
document.write(num);

document.write("<br>");
//---------------------
function detail(obj) {
  return (
    "我是" +
    obj.name +
    "今年" +
    obj.age +
    "歲" +
    ",性別" +
    obj.gender +
    "性,住在" +
    obj.address
  );
}
var who = {
  name: "孫悟空",
  age: 24,
  gender: "男",
  address: "龜仙島",
};
document.write(detail(who));

document.write("<br>");
//---------------------

function fun(a) {
  document.write("a=" + a);
}

fun(function () {
  document.write("hello");
});

document.write("<br>");

function circle(num) {
  return num * num * 3.14;
}

fun(circle(10));
