//轉為string
var num1 = 123;
num1 = num1 + "";
console.log(typeof num1);
console.log("num1 =" + num1);

//順序
var num2 = 1 + 2 + "3";
var num3 = "1" + 2 + 3;
console.log("num2 =" + num2);
console.log("num3 =" + num3);

//除了加法其餘轉成數字
var num4 = "200" - 100;
console.log("num4 =" + num4);

var num5 = "213";
num5 = num5 - 1;
console.log(num5);
