//數字
var b = 0;
b = Boolean(b);
console.log(b);

var num2 = NaN;
num2 = Boolean(num2);
console.log(num2);

var num1 = 123;
num1 = Boolean(num1);
console.log(num1);

//字串
var str1 = "false";
str1 = Boolean(str1);
console.log(str1);

var str2 = "";
str2 = Boolean(str2);
console.log(str2);

var str3 = null;
str3 = Boolean(str3);
console.log(str3);

var str4 = undefined;
str4 = Boolean(str4);
console.log(str4);
