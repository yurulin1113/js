var a = "321";
var b = 123;

//Number()
a = Number(a);
console.log(typeof a);
console.log(a + b);

//parseInt()有效整數取得
var c = "12325px";
c = parseInt(c);
console.log(c);

var d = "s1515616c"; //不合法
d = parseInt(d);
console.log(d);

var e = "151.ffe5616c"; //小數點後不取
e = parseInt(e);
console.log(e);

//parsefloat()取得有效小數
var num = "123.456.789px";
num = parseFloat(num);
console.log(num);

//boolean
var x = true;
x = Number(x);
console.log(x);

x = false;
x = Number(x);
console.log(x);

x = null;
x = Number(x);
console.log(x);

//非數字
var y = "123x";
y = Number(y);
console.log(y);
