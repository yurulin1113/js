function fun1() {
  function fun2() {
    document.write("我是fun2<br>");
  }
  return fun2;
}

var a = fun1();
document.write(a + "<br>");

fun1()();
//-----------------------------------
(function (a, b) {
  document.write("a=" + a);
  document.write("b=" + b);
})(155, 131);
