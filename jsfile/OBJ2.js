//互相受影響 引用數據
var obj1 = new Object();

obj1.name = "孫悟空";

var obj2 = obj1;

obj2.name = "唐三藏";
obj2 = null; //影響

console.log(obj1);
console.log(obj2);

var c = 10;
var d = 10;
document.writeln(c == d);

document.writeln("<br>-------------------------<br> ");

var obj3 = new Object();
var obj4 = new Object();

obj3.name = "悟天";
obj4.name = "悟天";

document.writeln(obj3 == obj4); //false不同對象
