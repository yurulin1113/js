var a = 10;
console.log("10" == a); //string轉成數字
console.log(null == 0);
console.log(undefined == null);

console.log(NaN == NaN); //NaN不跟人相等

var b = NaN;
console.log(isNaN(b)); //isNaN返回是否為NaN

console.log("abcd" == "abcd");

console.log(1 !== "1"); //不全等
console.log(1 != "1"); //不等
console.log(1 === "1"); //全等
