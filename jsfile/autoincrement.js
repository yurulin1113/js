var n1 = 10,
  n2 = 20;

var n = n1++;
console.log(n); //10
console.log(n1); //11

n = ++n1;
console.log(n); //12
console.log(n1); //12

console.log("-------------------------------------------------");

n = n2--;
console.log(n); //20
console.log(n2); //19

n = --n2;
console.log(n); //18
console.log(n2); //18
