//類型檢查
var x = 123;
var y = "123";
console.log(typeof x);
console.log(typeof y);

//最大值
console.log(Number.MAX_VALUE);

//最小值0以上
console.log(Number.MIN_VALUE);

//無限
var a = Number.MAX_VALUE * Number.MAX_VALUE;
console.log(typeof a);

var b = Infinity;
console.log(typeof b);

//負無限
var c = -Number.MAX_VALUE * Number.MAX_VALUE;
console.log(c);

//字串相乘NaN=Not A Number
var s;
s = "abc" * "bcd";
console.log(s);

s = NaN;
console.log(typeof s);

//運算
var num1;
num1 = 15615 + 56;
console.log(num1);

var num2;
num2 = 0.1 + 0.2;
console.log(num2); //小數
