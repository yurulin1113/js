// |+ &-
var result = true || false;
console.log(result);

true && alert("ㄏㄏ");
false || alert("跳");

var x = 1 && 2;
console.log(x);

var y = 1 || 2;
console.log(y);

console.log("--------------------");

//0=false NaN=false
var num1 = 0 && 2;
console.log(num1);

var num2 = 0 || 2;
console.log(num2);

//都false跳前面
var num3 = NaN && 0;
console.log(num3);

var num4 = 0 && NaN;
console.log(num4);

console.log("--------------------");
console.log(1 > true);
console.log(10 > "ss");
console.log("1" > "5");
console.log("5" > "1");

//ASCII
console.log("a" > "b");
console.log("bc" > "b");
console.log("bc" > "ad");
console.log("\u2620");
