var a = 10;

function fun() {
  var a = "我是fun的變量a";
  var b = 20;

  function fun2() {
    document.write("a=" + window.a); //抓全局變數
  }

  fun2();
}

fun();

document.write("<br>");
//-----------------------------
var c = 100;

function fun3() {
  document.write("c =" + c);
}

fun3(); //函數沒有數字抓全局

document.write("<br>");
//------------------------------
function fun4() {
  document.write("c =" + c);
  var c = 99; //函數值undefinded局部
}
fun4();

document.write("<br>");
//--------------------------------
function fun5() {
  document.write("c =" + c);
  c = 98; //沒var算全局改了
}
fun5();

document.write("<br>");
//------------------------------------
document.write("c =" + c); //沒var算全局
