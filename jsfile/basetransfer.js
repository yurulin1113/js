//16進制0x
var a;
a = 0x10;
console.log(a);

//8進制0
var b;
b = 070;
console.log(b);

//2進制0b
var c;
c = 0b110;
console.log(c);

//parseInt(x,y)指定進制
d = "070";

d = parseInt(d, 10);

console.log(d);
