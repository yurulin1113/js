var a = 123;
console.log(typeof a);

//一.直接給toSting()
a = true;
a = a.toString();
console.log(typeof a);
console.log(a);

var b = a.toString();
console.log(typeof b);

//二.String()
var c = 1234;
var d = String(c);
console.log(typeof d);
