var num1 = 123;
num1 = +num1;
console.log("num1 =" + num1);

//++轉換為num
var num2 = 1 + +"2" + 3;
console.log("num2 =" + num2);

//自增
var x = 1;
console.log("x++ =" + x++); //x++原來的數

var y = 1;
console.log("++y =" + ++y); //++y

var z = 20;
z = z++ + ++z + z; //20+22+22 z++之後才變21
console.log("20+22+22 =" + z);

console.log("--------------------------------------------");

//自減
var n1 = 100;
console.log("n1-- =" + n1--);

var n2 = 100;
console.log("--n1 =" + --n2);

var n3 = 100;
n3 = n3-- + --n3 + n3; //100+98+98
console.log("100+98+98 =" + n3);
